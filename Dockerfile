FROM wodby/varnish:6

HEALTHCHECK --start-period=3s --interval=30s --retries=2 --timeout=1s \
  CMD curl -f localhost:6081/.vchealthz 

# https://github.com/ochinchina/supervisord
# used to start both varnish and exporter
COPY supervisord.conf /etc/supervisord.conf
COPY --from=registry.gitlab.com/opencity-labs/supervisord:v0.7.3 /usr/local/bin/supervisord \
        /usr/local/bin/supervisord

COPY --from=umsp/prometheus_varnish_exporter:0.1.1 /go/bin/prometheus_varnish_exporter /usr/local/bin/prometheus_varnish_exporter


COPY templates /etc/gotpl/

CMD [ "/usr/local/bin/supervisord" ]

EXPOSE 9131 9001
