# Varnish Docker Container Image

The super-useful Wodby/Varnish docker image with prometheus monitoring in the same container

## Ingredients

* [wodby/varnish](https://github.com/wodby/varnish)
* [jonnenauha/prometheus_varnish_exporter](https://github.com/jonnenauha/prometheus_varnish_exporter)
* [ochinchina/supervisord](https://github.com/ochinchina/supervisord)

## Running

Follow the same rule of the original wodby image, but exposes two more ports for monitoring
* 6081 & 6082 standard http and admin ports for varnish
* 9131 where you'll find varnish metrics
* 9001 where you'll find supervisord metrics

## Other changes

- Added possibility to setup probes of backend:

| Variable                  | Default | Description                        |
|---------------------------|---------|------------------------------------|
| `VARNISH_BACKEND_PROBE_URL` |         | if set, varnish starts probing backend 
| `VARNISH_BACKEND_PROBE_TIMEOUT ` | 1s | timeout of the probe from varnish to the backend
| `VARNISH_BACKEND_PROBE_INTERVAL` | 5s | interval between probes |
| `VARNISH_BACKEND_PROBE_WINDOW`   | 5  | number of probes to evaluate
| `VARNISH_BACKEND_PROBE_THRESHOLD` | 3 | number of success probes in the window that needs to success to consider backend healthy

